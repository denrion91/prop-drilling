import React from 'react';
import { useParams } from 'react-router-dom';

const MoviePage = ({ movies }) => {
  const { title: movieTitle } = useParams();

  const movieToRender = movies.find(movie => movie.title === movieTitle);

  if (!movieToRender) {
    return <div>No movie data to render</div>;
  }

  return <div>{movieToRender.title}</div>;
};

export default MoviePage;
