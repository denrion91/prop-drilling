import { useState } from 'react';
import { BrowserRouter as Router, Link, Route } from 'react-router-dom';
import MovieList from './components/MovieList';
import MoviePage from './pages/MoviePage';

const App = () => {
  const [movies, setMovies] = useState([]);

  return (
    <Router>
      <nav>
        <ul>
          <li>
            <Link to='/'>Home</Link>
          </li>
        </ul>
      </nav>

      <Route path='/' exact>
        <MovieList movies={movies} setMovies={setMovies} />
      </Route>
      <Route path='/movies/:title' exact>
        <MoviePage movies={movies} />
      </Route>
    </Router>
  );
};

export default App;
