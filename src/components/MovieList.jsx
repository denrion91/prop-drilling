import React, { useEffect } from 'react';
import { fetchMovies } from '../api/movies';
import MovieItem from './MovieItem';

const MovieList = ({ movies, setMovies }) => {
  useEffect(() => {
    fetchMovies().then(data => setMovies(data));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div>
      <h1>Movies</h1>
      <ul>
        {movies.map(movie => (
          <MovieItem key={movie.title} movie={movie} />
        ))}
      </ul>
    </div>
  );
};

export default MovieList;
