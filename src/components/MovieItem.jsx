import React from 'react';
import { Link } from 'react-router-dom';

const MovieItem = ({ movie }) => {
  return (
    <li>
      <h3>
        <Link to={`/movies/${movie.title}`}>{movie.title}</Link>
      </h3>
    </li>
  );
};

export default MovieItem;
