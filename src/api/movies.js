export const fetchMovies = async () => {
  return fetch('https://swapi.dev/api/films')
    .then(response => {
      return response.json();
    })
    .then(jsonResponse => {
      console.log('jsonResponse', jsonResponse);
      return jsonResponse.results;
    })
    .catch(console.error);
};
